package br.edu.catolicato.viwork.adapter;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import br.edu.catolicato.viwork.R;
import br.edu.catolicato.viwork.modelo.Postagem;

/**
 * Created by Luídne on 18/07/2015.
 */
public class PostagemAdapter extends RecyclerView.Adapter<PostagemAdapter.PostagemViewHolder> {

    private Fragment context;
    private List<Postagem> itens;

    public PostagemAdapter(Fragment context, List<Postagem> itens) {
        this.context = context;
        this.itens = itens;
    }

    @Override
    public PostagemAdapter.PostagemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context.getActivity()).inflate(R.layout.item_postagem, parent, false);
        PostagemViewHolder viewHolder = new PostagemViewHolder(context, view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PostagemViewHolder holder, int position) {
        final Postagem postagem = itens.get(position);

        holder.data.setText(DateUtils.getRelativeTimeSpanString(postagem.getData().getTime(), Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS));
        holder.legenda.setText(postagem.getTexto());
    }

    @Override
    public int getItemCount() {
        return this.itens.size();
    }

    public Postagem getItem(int position) {
        return itens.get(position);
    }

    public void addItems(List<Postagem> lista) {
        this.itens.addAll(lista);
        this.notifyDataSetChanged();
    }

    public void removeItems() {
        this.itens.clear();
        this.notifyDataSetChanged();
    }

    public static class PostagemViewHolder extends RecyclerView.ViewHolder {

        private Fragment context;
        public View itemView;

        public final TextView data;
        public final TextView legenda;
        public final ProgressBar progressBar;
        public final View textViewFalha;

        public PostagemViewHolder(Fragment context, final View itemView) {
            super(itemView);

            this.context = context;
            this.itemView = itemView;

            this.data = (TextView) itemView.findViewById(R.id.textViewData);
            this.legenda = (TextView) itemView.findViewById(R.id.textViewTexto);
            this.progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar2);
            this.textViewFalha = itemView.findViewById(R.id.textViewFalha);
        }
    }
}