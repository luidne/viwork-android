package br.edu.catolicato.viwork.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.edu.catolicato.viwork.R;
import br.edu.catolicato.viwork.activity.PublicarActivity;
import br.edu.catolicato.viwork.adapter.PostagensHeaderFooter;
import br.edu.catolicato.viwork.modelo.Postagem;
import br.edu.catolicato.viwork.modelo.PostagemServicoREST;
import br.edu.catolicato.viwork.util.EndlessRecyclerOnScrollListener;

public class TimelineFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private SwipeRefreshLayout pullRefreshLayout;
    private PostagensHeaderFooter postagemAdapter;
    private ProgressBar footerRecycler;

    public static TimelineFragment newInstance() {
        return new TimelineFragment();
    }

    public TimelineFragment() {}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.pullRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        this.pullRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullRefreshLayout.setRefreshing(true);
                new Buscador().execute();
            }
        });

        this.fab = (FloatingActionButton) view.findViewById(R.id.fab);
        this.fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Snackbar.make(getDrawerActivity().findViewById(R.id.coordinator_layout), "FAB Clicked", Snackbar.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), PublicarActivity.class));
            }
        });

        setList();
    }

    private void setList() {
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.listPostagens);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // TO DO
                // Carregar postagens sob demanda
                Log.d("onLoadMore", "currentPage" + current_page);
            }
        });

        postagemAdapter = new PostagensHeaderFooter(getActivity(), new ArrayList<Postagem>());
        recyclerView.setAdapter(postagemAdapter);

    }

    @Override
    protected int getTitle() {
        return R.string.navigation_item_timeline;
    }

    @Override
    public boolean hasCustomToolbar() {
        return true;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_postagens;
    }

    class Buscador extends AsyncTask<Void,Void, List<Postagem>> {

        @Override
        protected List<Postagem> doInBackground(Void... voids) {
            PostagemServicoREST rest = new PostagemServicoREST();
            try {
                return rest.getTodos();
            } catch (IOException e) {
                e.printStackTrace();
                return new ArrayList<>(0);
            }
        }

        @Override
        protected void onPostExecute(List<Postagem> postagens) {
            super.onPostExecute(postagens);
            postagemAdapter = new PostagensHeaderFooter(getActivity(), postagens);
            recyclerView.setAdapter(postagemAdapter);
        }
    }

}
