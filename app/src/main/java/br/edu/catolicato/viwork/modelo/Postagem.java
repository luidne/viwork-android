package br.edu.catolicato.viwork.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

/**
 * Created by Luídne on 30/10/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Postagem {
    private int id;
    private String texto;
    private Date data;

    public Postagem() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
